package com.parkinglot;

import java.util.List;

public class ParkingBoy {
    protected List<ParkingLot> parkingLots;
    public ParkingBehavior parkingBehavior;

    public ParkingBoy(List<ParkingLot> parkingLots, ParkingBehavior parkingBehavior) {
        this.parkingLots = parkingLots;
        this.parkingBehavior = parkingBehavior;
    }

    public Car fetchCar(Ticket ticket) {
        ParkingLot findParkingLot = parkingLots.stream().filter(parkingLot -> parkingLot.isValidTicket(ticket)).findFirst().orElse(parkingLots.get(0));
        return findParkingLot.fetchCar(ticket);
    }

    public Ticket parkCar(Car car) {
        return parkingBehavior.parkCar(parkingLots, car);
    }
}
