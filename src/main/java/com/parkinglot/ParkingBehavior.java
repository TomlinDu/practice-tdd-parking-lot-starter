package com.parkinglot;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.Ticket;

import java.util.List;

public interface ParkingBehavior {
    public Ticket parkCar(List<ParkingLot> parkingLots, Car car);
}
