package com.parkinglot;

import java.util.List;

public class SuperSmartParkingBehavior implements ParkingBehavior {
    @Override
    public Ticket parkCar(List<ParkingLot> parkingLots, Car car) {
        ParkingLot smartFindParkingLot = parkingLots.stream().
                reduce((preParkingLot, nowParkingLot) -> preParkingLot.vancantSpaceRatioLargerThan(nowParkingLot) ? preParkingLot : nowParkingLot).get();
        return smartFindParkingLot.parkCar(car);
    }
}
