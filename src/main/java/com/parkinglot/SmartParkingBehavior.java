package com.parkinglot;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.Ticket;

import java.util.List;

public class SmartParkingBehavior implements ParkingBehavior {
    @Override
    public Ticket parkCar(List<ParkingLot> parkingLots, Car car) {
        ParkingLot smartFindParkingLot = parkingLots.stream().reduce((preParkingLot, nowParkingLot) -> preParkingLot.vancantSpaceLargerThan(nowParkingLot) ? preParkingLot : nowParkingLot).get();
        return smartFindParkingLot.parkCar(car);
    }
}
