package com.parkinglot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ParkingLot {
    protected int capacity;
    protected int id;
    protected List<Car> cars = new ArrayList<>();
    protected HashMap<Ticket, Car> parkingTicketMap;

    public ParkingLot(int capacity) {
        this.capacity = capacity;
        this.parkingTicketMap = new HashMap<>();
    }

    public ParkingLot(int capacity, int id) {
        this.capacity = capacity;
        this.id = id;
        parkingTicketMap = new HashMap<>();
    }

    public int getId() {
        return id;
    }

    public Ticket parkCar(Car car) {
        if (this.isFull()) {
            throw new RuntimeException("No available position");
        }
        cars.add(car);
        Ticket ticket = new Ticket(id);
        parkingTicketMap.put(ticket, car);
        return ticket;
    }

    public boolean isFull() {
        return parkingTicketMap.size() == capacity;
    }

    public boolean isValidTicket(Ticket parkingTicket) {
        return parkingTicketMap.containsKey(parkingTicket);
    }

    public Car fetchCar(Ticket ticket) {
        if (!isValidTicket(ticket) || ticket.isUsed()) {
            throw new RuntimeException("Unrecognized parking ticket");
        }
        ticket.useTicket();
        return parkingTicketMap.remove(ticket);
    }

    public boolean vancantSpaceLargerThan(ParkingLot parkingLot) {
        return this.capacity - this.parkingTicketMap.size() >= parkingLot.capacity - parkingLot.parkingTicketMap.size();
    }

    public boolean vancantSpaceRatioLargerThan(ParkingLot parkingLot) {
        return ((this.capacity - this.parkingTicketMap.size()) * 1.0 / this.capacity) >= ((parkingLot.capacity - parkingLot.parkingTicketMap.size()) * 1.0 / parkingLot.capacity);
    }
}
