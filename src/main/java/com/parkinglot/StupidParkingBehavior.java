package com.parkinglot;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.Ticket;

import java.util.List;


public class StupidParkingBehavior implements ParkingBehavior {
    @Override
    public Ticket parkCar(List<ParkingLot> parkingLots, Car car) {
        ParkingLot findParkingLot = parkingLots.stream().filter(parkingLot -> !parkingLot.isFull()).findFirst().orElse(parkingLots.get(0));
        return findParkingLot.parkCar(car);
    }
}
