package com.parkinglot;

public class Ticket {
    protected boolean beUsed;
    protected int relatedParkingLotId;

    public Ticket(int group) {
        this.beUsed = false;
        this.relatedParkingLotId = group;
    }

    public int getRelatedParkingLotId() {
        return relatedParkingLotId;
    }

    public void useTicket() {
        this.beUsed = true;
    }

    public boolean isUsed() {
        return this.beUsed;
    }

}
