package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.management.InvalidApplicationException;
import java.security.UnrecoverableEntryException;

public class ParkingLotTest {
    @Test
    void should_return_a_parking_ticket_when_parkCar_given_a_car() {
        //Given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        //When
        Ticket ticket = parkingLot.parkCar(car);
        //Then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_two_different_parking_tickets_when_parkCar_given_two_different_cars() {
        //Given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        //When
        Ticket ticket1 = parkingLot.parkCar(car1);
        Ticket ticket2 = parkingLot.parkCar(car2);
        //Then
        Assertions.assertNotNull(ticket1);
        Assertions.assertNotNull(ticket2);
        Assertions.assertNotEquals(ticket1, ticket2);
    }

    @Test
    void should_return_the_parked_car_when_fetchCar_given_ticket() {
        //Given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        Ticket ticket = parkingLot.parkCar(car);
        //When
        Car fetchedCar = parkingLot.fetchCar(ticket);
        //Then
        Assertions.assertNotNull(fetchedCar);
        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_two_different_cars_when_fetchCar_given_two_tickets() {
        //Given
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        Ticket ticket1 = parkingLot.parkCar(car1);
        Ticket ticket2 = parkingLot.parkCar(car2);
        //When
        Car fetchedCar1 = parkingLot.fetchCar(ticket1);
        Car fetchedCar2 = parkingLot.fetchCar(ticket2);
        //Then
        Assertions.assertNotNull(fetchedCar1);
        Assertions.assertNotNull(fetchedCar2);
        Assertions.assertNotEquals(fetchedCar1, fetchedCar2);
    }

    @Test
    void should_return_message_No_available_position_when_parkCar_given_parking_lot_is_full() {
        //Given
        Car car1 = new Car();
        ParkingLot parkingLot = new ParkingLot(1);
        Ticket ticket1 = parkingLot.parkCar(car1);
        Car car2 = new Car();
        //When

        //Then
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingLot.parkCar(car2));
        Assertions.assertEquals("No available position", exception.getMessage());
    }

    @Test
    void should_return_message_Unrecognized_parking_ticket_when_fectchCar_given_ticket_is_invalid() {
        //Given
        ParkingLot parkingLot = new ParkingLot(10);
        Ticket ticket = new Ticket(-1);
        //When

        //Then
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingLot.fetchCar(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_message_Unrecognized_parking_ticket_when_fectchCar_given_ticket_is_used() {
        //Given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        Ticket ticket = parkingLot.parkCar(car);
        parkingLot.fetchCar(ticket);
        //When

        //Then
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingLot.fetchCar(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", exception.getMessage());
    }


}
