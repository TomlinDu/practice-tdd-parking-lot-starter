package com.parkinglot;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.util.List;

public class ParkingBoyTest {
    @Test
    void should_return_ticket_when_parkCar_given_parking_lot_and_parking_boy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        Car car = new Car();
        //when
        Ticket ticket = parkingBoy.parkCar(car);
        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetchCar_given_parking_lot_and_parking_boy_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        Car car = new Car();
        Ticket parkingTicket = parkingBoy.parkCar(car);
        //when
        Car fetchedCar = parkingLot.fetchCar(parkingTicket);
        //then
        assertNotNull(fetchedCar);
    }

    @Test
    void should_return_right_car_with_each_ticket_when_fetch_given_parking_lot_with_two_parked_cars_and_parking_boy_and_two_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket parkingTicket1 = parkingBoy.parkCar(car1);
        Ticket parkingTicket2 = parkingBoy.parkCar(car2);
        //when
        Car returnCar1 = parkingBoy.fetchCar(parkingTicket1);
        Car returnCar2 = parkingBoy.fetchCar(parkingTicket2);
        //then
        assertEquals(car1, returnCar1);
        assertEquals(car2, returnCar2);
        assertNotEquals(returnCar1, returnCar2);
    }

    @Test
    void should_return_nothing_when_fetch_given_parking_lot_and_parking_boy_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        Car car = new Car();
        parkingBoy.parkCar(car);
        Ticket wrongParkingTicket = new Ticket(parkingLot.id);
        //when
        //then
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkingBoy.fetchCar(wrongParkingTicket));
        assertEquals("Unrecognized parking ticket", exception.getMessage());

    }

    @Test
    void should_return_nothing_when_fetch_given_parking_lot_and_parking_boy_and_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        Car car = new Car();
        Ticket parkingTicket = parkingBoy.parkCar(car);
        parkingBoy.fetchCar(parkingTicket);
        //when
        //then
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkingBoy.fetchCar(parkingTicket));
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_nothing_when_park_given_parking_lot_without_any_position_and_parking_boy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        parkingBoy.parkCar(new Car());
        Car car = new Car();
        //when
        //then
        RuntimeException exception = assertThrows(RuntimeException.class, () -> parkingBoy.parkCar(car));
        assertEquals("No available position", exception.getMessage());
    }
}
