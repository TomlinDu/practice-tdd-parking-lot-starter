package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SuperParkingBoyTest {
    @Test
    void should_car_park_to_the_first_parking_lot_when_park_given_two_parking_lots_both_with_available_position_and_lot1_vacant_capacity_ratio_greater_than_lot2_vacant_capacity_ratio_and_super_smart_parking_boy_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10, 1);
        ParkingLot parkingLot2 = new ParkingLot(9, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2), new SuperSmartParkingBehavior());
        superSmartParkingBoy.parkCar(new Car());
        superSmartParkingBoy.parkCar(new Car());
        Car car = new Car();
        //when
        Ticket parkingTicket = superSmartParkingBoy.parkCar(car);
        //then
        assertEquals(parkingTicket.getRelatedParkingLotId(), parkingLot1.getId());
    }

    @Test
    void should_car_park_to_the_second_parking_lot_when_park_given_two_parking_lots_both_with_available_position_and_lot2_vacant_capacity_ratio_greater_than_lot1_vacant_capacity_ratio_and_super_smart_parking_boy_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10, 1);
        ParkingLot parkingLot2 = new ParkingLot(11, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2), new SuperSmartParkingBehavior());
        superSmartParkingBoy.parkCar(new Car());
        superSmartParkingBoy.parkCar(new Car());
        Car car = new Car();
        //when
        Ticket parkingTicket = superSmartParkingBoy.parkCar(car);
        //then
        assertEquals(parkingTicket.getRelatedParkingLotId(), parkingLot2.getId());
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_the_car_twice_given_super_smart_parking_boy_who_manage_two_parking_lots_both_with_a_parked_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10, 1);
        ParkingLot parkingLot2 = new ParkingLot(10, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2), new SuperSmartParkingBehavior());
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket parkingTicket1 = superSmartParkingBoy.parkCar(car1);
        Ticket parkingTicket2 = superSmartParkingBoy.parkCar(car2);
        //when
        Car returnCar1 = superSmartParkingBoy.fetchCar(parkingTicket1);
        Car returnCar2 = superSmartParkingBoy.fetchCar(parkingTicket2);
        //then
        assertEquals(car1, returnCar1);
        assertEquals(car2, returnCar2);
    }

    @Test
    void should_return_nothing_with_error_message_Unrecognized_parking_ticket_when_fetch_given_super_smart_parking_boy_who_manage_two_parking_lots_and_an_unrecognized_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1, 1);
        ParkingLot parkingLot2 = new ParkingLot(10, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2), new SuperSmartParkingBehavior());
        Ticket wrongParkingTicket = new Ticket(-1);
        //when
        //then
        RuntimeException exception = assertThrows(RuntimeException.class, () -> superSmartParkingBoy.fetchCar(wrongParkingTicket));
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_nothing_with_error_message_Unrecognized_parking_ticket_when_fetch_given_super_smart_parking_boy_who_manage_two_parking_lots_and_an_used_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10, 1);
        ParkingLot parkingLot2 = new ParkingLot(10, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2), new SuperSmartParkingBehavior());
        Ticket usedParkingTicket = superSmartParkingBoy.parkCar(new Car());
        superSmartParkingBoy.fetchCar(usedParkingTicket);
        //when
        //then
        RuntimeException exception = assertThrows(RuntimeException.class, () -> superSmartParkingBoy.fetchCar(usedParkingTicket));
        assertEquals("Unrecognized parking ticket", exception.getMessage());
    }

    @Test
    void should_return_nothing_with_error_message_No_available_position_when_park_given_super_smart_parking_boy_who_manage_two_parking_lots_and_both_without_any_position_and_a_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1, 1);
        ParkingLot parkingLot2 = new ParkingLot(1, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2), new SuperSmartParkingBehavior());
        superSmartParkingBoy.parkCar(new Car());
        superSmartParkingBoy.parkCar(new Car());
        //when
        //then
        RuntimeException exception = assertThrows(RuntimeException.class, () -> superSmartParkingBoy.parkCar(new Car()));
        assertEquals("No available position", exception.getMessage());
    }
}
